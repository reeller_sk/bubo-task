<?php
/**
 * @file
 * bubo_taxonomy_vocabularies.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bubo_taxonomy_vocabularies_taxonomy_default_vocabularies() {
  return array(
    'contexts' => array(
      'name' => 'Contexts',
      'machine_name' => 'contexts',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'projects' => array(
      'name' => 'Projects',
      'machine_name' => 'projects',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'status' => array(
      'name' => 'Status',
      'machine_name' => 'status',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
