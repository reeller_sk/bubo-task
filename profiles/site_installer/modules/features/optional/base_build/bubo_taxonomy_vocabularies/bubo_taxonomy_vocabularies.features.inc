<?php
/**
 * @file
 * bubo_taxonomy_vocabularies.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bubo_taxonomy_vocabularies_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}
