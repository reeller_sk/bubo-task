<?php
/**
 * @file
 * bubo_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bubo_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_add-context:admin/structure/taxonomy/contexts/add
  $menu_links['main-menu_add-context:admin/structure/taxonomy/contexts/add'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/taxonomy/contexts/add',
    'router_path' => 'admin/structure/taxonomy/%/add',
    'link_title' => 'Add Context',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_add-context:admin/structure/taxonomy/contexts/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_add-project:admin/structure/taxonomy/projects/add
  $menu_links['main-menu_add-project:admin/structure/taxonomy/projects/add'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/taxonomy/projects/add',
    'router_path' => 'admin/structure/taxonomy/%/add',
    'link_title' => 'Add Project',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_add-project:admin/structure/taxonomy/projects/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_add-task:node/add/task
  $menu_links['main-menu_add-task:node/add/task'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/task',
    'router_path' => 'node/add/task',
    'link_title' => 'Add Task',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_add-task:node/add/task',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-sidebar-menu_:<view>
  $menu_links['menu-sidebar-menu_:<view>'] = array(
    'menu_name' => 'menu-sidebar-menu',
    'link_path' => '<view>',
    'router_path' => '<view>',
    'link_title' => '',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_views' => array(
        'mlid' => 524,
        'type' => 'view',
        'original_path' => '',
        'view' => array(
          'name' => 'projects',
          'display' => 'block_1',
          'arguments' => '',
          'settings' => array(
            'wrapper_classes' => 'menu-views',
            'breadcrumb' => 1,
            'breadcrumb_title' => '',
            'breadcrumb_path' => '<front>',
            'title' => 0,
            'title_wrapper' => '',
            'title_classes' => '',
            'title_override' => '',
          ),
        ),
      ),
      'identifier' => 'menu-sidebar-menu_:<view>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-sidebar-menu_do-it:tasks/do-it
  $menu_links['menu-sidebar-menu_do-it:tasks/do-it'] = array(
    'menu_name' => 'menu-sidebar-menu',
    'link_path' => 'tasks/do-it',
    'router_path' => 'tasks/do-it',
    'link_title' => 'Do It',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-sidebar-menu_do-it:tasks/do-it',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-sidebar-menu_tasks:tasks
  $menu_links['menu-sidebar-menu_tasks:tasks'] = array(
    'menu_name' => 'menu-sidebar-menu',
    'link_path' => 'tasks',
    'router_path' => 'tasks',
    'link_title' => 'Tasks',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-sidebar-menu_tasks:tasks',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-sidebar-menu_trash:tasks/trash
  $menu_links['menu-sidebar-menu_trash:tasks/trash'] = array(
    'menu_name' => 'menu-sidebar-menu',
    'link_path' => 'tasks/trash',
    'router_path' => 'tasks/trash',
    'link_title' => 'Trash',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-sidebar-menu_trash:tasks/trash',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add Context');
  t('Add Project');
  t('Add Task');
  t('Do It');
  t('Tasks');
  t('Trash');


  return $menu_links;
}
