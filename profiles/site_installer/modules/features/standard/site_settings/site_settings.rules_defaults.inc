<?php
/**
 * @file
 * site_settings.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function site_settings_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_anonymous_users_to_login'] = entity_import('rules_config', '{ "rules_redirect_anonymous_users_to_login" : {
      "LABEL" : "Redirect anonymous users to login",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "NOT data_is" : {
            "data" : [ "site:current-page:path" ],
            "op" : "IN",
            "value" : { "value" : [ "user\\/login", "user\\/register", "user\\/password" ] }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "user\\/login", "destination" : 1 } } ]
    }
  }');
  return $items;
}
