<?php
/**
 * @file
 * contexts_base.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function contexts_base_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = 'The basic Site-wide Context which applies to ALL sections of the site.';
  $context->tag = 'EPIC: STANDARD';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~user/*' => '~user/*',
        '~user' => '~user',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'menu-menu-sidebar-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-sidebar-menu',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('EPIC: STANDARD');
  t('The basic Site-wide Context which applies to ALL sections of the site.');
  $export['sitewide'] = $context;

  return $export;
}
