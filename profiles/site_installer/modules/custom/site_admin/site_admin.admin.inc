<?php
/**
 * Internal configuration form for site_admin
  */
function _site_admin_config_form() {
  $form['site_admin_tabs_hide'] = array(
    '#title' =>
    t('Hide page tabs'),
    '#description' =>
      t('Hide page tabs unless given special permission to see them.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('site_admin_tabs_hide', TRUE),
  );
  
  $form['site_admin_action_links_hide'] = array(
    '#title' =>
      t('Hide action links'),
    '#description' =>
      t('Hide page page action links.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('site_admin_action_links_hide', TRUE),
  );

  $form['site_admin_apply_these_settings_to_uid_1'] = array(
    '#title' =>
      t('Apply these settings to the UID:1 too'),
    '#description' =>
      t('Apply these \'hide\' settings to the Super User (UID:1) so that they apply during development too.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('site_admin_apply_these_settings_to_uid_1', TRUE),
  );
  
  return system_settings_form($form);
}