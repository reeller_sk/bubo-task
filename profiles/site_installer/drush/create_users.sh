drush user-create "Researcher" --mail="you+@youremail.com" --password="password"
drush user-create "Research lead" --mail="you++@youremail.com" --password="password"
drush user-create "User manager" --mail="you+++@youremail.com" --password="password"
drush user-add-role "Researcher" --name="Researcher"
drush user-add-role "Research lead" --name="Research lead"
drush user-add-role "User manager" --name="User manager"