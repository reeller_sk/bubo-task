/**
 * @file
 * JavaScript to disable and re-label the installer's submit button on the
 * Welcome screen after the first submit.
 */

(function ($) {
    Drupal.behaviors.installerWelcomeForm = {
        attach: function (context, settings) {
            $('#edit-submit').click(function() {
                var $this = $(this);

                // Replace the button with some text
                $this.val('Installing...');

                // Submit the form
                 $('#site-installer-welcome-form').submit();

                // Disable the button
                $this.attr("disabled","disabled");

                // Allow the click event to fall through
                return true;
            });
        }
    }
})(jQuery);