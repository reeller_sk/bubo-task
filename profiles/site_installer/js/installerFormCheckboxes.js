/**
 * @file
 * JavaScript to enable the various 'Select all' checkboxes on installer's
 * Optional Features screen.
 */

(function ($) {
    Drupal.behaviors.installerFormCheckboxes = {
        attach: function (context, settings) {
            $('#edit-check-all-checkbox').click(function() {
                var $this = $(this);
                // $this will contain a reference to the checkbox
                if ($this.is(':checked')) {
                    $('#edit-features-list input[type=checkbox]').attr( "checked", "checked" );
                } else {
                    $('#edit-features-list input[type=checkbox]').attr( "checked", "" );
                }
            });
            
            $('input.check-all').click(function() {
				
                var $this = $(this);
                var parent = $this.parent().parent();
                
                if ($this.is(':checked')) {
                    $('input.form-checkbox', parent).not('.check-all').attr( "checked", "checked" );
                } else {
                    $('input.form-checkbox', parent).not('.check-all').attr( "checked", "" );
                }
            });
        }
    }
})(jQuery);
