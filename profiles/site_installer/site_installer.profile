<?php
//Core modules that we want to enable
function _site_installer_core_modules()
{
    return array(
        'block',
        'comment',
        'menu',
        'taxonomy',
    );
}

function _site_installer_optional_perennials_modules()
{
    return array(
        'admin_menu',
        'adminimal_admin_menu',
        'admin_views',
        'better_formats',
        'ckeditor',
        'context',
        'contextual',
        'ctools',
        'date_popup',
        'entity',
        'entityreference',
        'features',
        'libraries',
        'role_delegation',
        'rules',
        'safe_cache_form_clear',
        'site_admin',
        'taxonomy_access_fix',
        'token',
        'views',
        'views_infinite_scroll',
        'uuid',
        'uuid_features',
    );
}

function _site_installer_developer_modules()
{
    return array(
        'context_ui',
        'devel',
        'ds_ui',
        'field_ui',
        'filter_perms',
        'module_filter',
        'rules_admin',
        'views_ui',
    );
}